#!/usr/bin/ruby

	require 'net/ftp'

	# server A
	_A = Net::FTP.new 'server_a'
		_A.login 'user', 'pass'
		_A.chdir './tmp'

=begin
	# server B
	_B = FTP.connect 'server_b'
		_B.login 'user', 'pass'
		_B.chdir './testni'

	# @brief ftp synchronisation
	class Mover

		# @brief recursively scans directories and syncs files
		def self.move list

			list.each do |handle|

				# exists
				exists = Mover.exists( _B.sendcmd "stat #{handle}" )
				update = !exists || ( exists && ( _A.mdtm( handle ) > _B.mdtm( handle )))
				
				if update

					_B.putbinaryfile _A.getbinaryfile( handle )

				end

				# is dir
				if !Mover.is_file _A.nlst( handle )

					# make dir if not exists
					if !Mover.exists( _B.sendcmd "stat #{handle}" )
					
						_B.mkdir handle

					end
					
					# move to dir
					_B.chdir handle
					_A.chdir handle
					
					# rerun func for new dir
					Mover.move Mover.ls( _A.nlst )

					# up one level when done
					_B.chdir '../'

				end

			end

		end

		# @brief determine whether entry really exists, based on FTP stat result
		# @return bool
		def self.exists stat

			return stat.split("\n").count > 2

		end

		# @brief determine whether a file is a file or dir, based on FTP nlst result
		# @return bool
		def self.is_file nlst_result

			return nlst_result - ['.', '..'] == nlst_result

		end

		# @brief clip ./, ../ from nlst results
		# @return array
		def self.ls list

			return list - ['.', '..']

		end
	end

	# move Mover.ls( _A.nlst )
=end